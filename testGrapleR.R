# Run the following 6 lines the first time you use your system to run the experiments:
install.packages("httr", repos = "http://cran.us.r-project.org")
install.packages("RCurl", repos = "http://cran.us.r-project.org")
install.packages("jsonlite", repos = "http://cran.us.r-project.org")
install.packages("devtools", repos = "http://cran.us.r-project.org")

install.packages('git2r', repos = "http://cran.us.r-project.org")

library("devtools")
devtools::install_github("GRAPLE/GRAPLEr")

#Load Required Libraries
library(httr)
library(RCurl)
library(jsonlite)
library(GRAPLEr)

# Default SubmissionURL is set to https://graple.acis.ufl.edu, use setSubmissionURL method to change submissionURL

#Batch Experiment
#
#Start a new experiment and setup parameters
#Retention specifies the number of days to retain results on server. Set to 10 if not specified. 0 denotes that results be deleted after first download
#Set APIKey parameters to readLines from the key file. If you're a registered user, your default email will be used 
grapleExp1 <- new("Graple",
                  APIKey = "7RBOWKXMQXOYLHPQ9ZVZI5VQXB55D7VSTT411V4J58Q7Z4PUMNPTUA9TZRUGT9IG",
                  Retention = 5,
                  ExpRootDir = file.path(getwd(), 'eddie'),
                  ResultsDir = file.path(getwd(), 'results'),
                  TempDir = tempdir())
grapleExp1 <- setSubmissionURL(grapleExp1, "https://graple.acis.ufl.edu")
grapleExp1 <- setExpName(grapleExp1, "BatchExperiment1")

#Run the experiment
grapleExp1 <- GrapleRunExperiment(grapleExp1)
cat(grapleExp1@StatusMsg)

#check on status and wait until it is ready
grapleExp1 <- GrapleCheckExperimentCompletion(grapleExp1)
while (grapleExp1@StatusMsg != '100.0% complete') {
  Sys.sleep(5);
  grapleExp1 <- GrapleCheckExperimentCompletion(grapleExp1);
  cat(paste(grapleExp1@StatusMsg, "\n"));
}

#get the experiment results. Extracted to results dir you specified
grapleExp1 <- GrapleGetExperimentResults(grapleExp1);
cat(grapleExp1@StatusMsg);
